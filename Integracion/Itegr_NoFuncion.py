# -*- coding: utf-8 -*-
import os
import rawpy
import imageio
import cv2

from skimage import color, exposure                                   
from get_one_param import *
from Distribucion_Colores import *
from Distribucion_UV import *
from operaciones import *
from Recorta_img import *

#------------------------------Crear archivos .csv-----------------------------------
archivo_RGB = open("Porcetajes_RGB.csv", 'w')
archivo_RGB.write("ID,rojo,naranjo,amarillo,verde,celeste,azul,morado,rosado\n")
archivo_RGB.close()

archivo_UV = open("Porcetajes_UV.csv", 'w')
archivo_UV.write("ID,bajo,medio,alto\n")
archivo_UV.close()

#--------------------------------Cargar imagen raw--------------------------
dir = 'D:\PDI_DATASET\etiquetadas/' #carpeta donde se encuentran las imágenes a precesar
fotos = os.listdir(dir)
print(fotos)
for foto in fotos:
    print(foto)
    aux = foto.split(".") #aux contiene nombre del archivo y extencion [<nombre>,CRW]
    IDs=aux[0].split("_") #aux[0] contiene tipo y flores contenidas en imagen
    IDs=IDs[1:]
    #print(IDs)
    n_flores = len(IDs)
    print("hay",n_flores,"flores")
    if foto.endswith(".CRW"):
        #foto a leer
        foto_dir = dir+foto
        print(foto_dir)
        with rawpy.imread(foto_dir) as raw:
            rgb = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
        #rgb = rgb.astype(np.uint64)
        #rgb=multiplicativa(rgb,2)
        #rgb = rgb.astype(np.uint8)
        #output = aux[0]+".png"
        #imageio.imsave(output, rgb)

        #-----------------Verificar que tipo de foto es (UV o RGB)------------------#
        
        #Si es UV, convertir la foto en 1 canal (grises) con canal rojo
        img = rgb[:,:,0:3]
        if ("RGB" in foto):
            RGB = True
            print("RGB")
        else:
            RGB = False
            print("UV")
            #image = imread('img/f2_UV_brillo.png')
            img = rgb[:,:,0] # se usa canal R
            #plt.imshow(im_read)
            #plt.show()
            #im_read.shape
           
            
        #------------------------Identificacion barra de grises-----------------------#
        #pendiente    
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5,5), 0)
        adjusted = cv2.convertScaleAbs(blur, 3, 10)
        thresh = cv2.threshold(adjusted, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
        thresh = 255 - thresh
        kernel = np.ones((800,70), np.uint8)
        morph = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        masked = gray.copy()
        morph = cv2.bitwise_not(morph)
        masked = cv2.bitwise_and(masked, masked, mask = morph)
        nonZeroMask = np.all(masked !=0 , axis=-1)
        nonZero = masked[nonZeroMask]
        meanBrightness = np.mean(nonZero)

        #------------------------Normalizacion (y aumento) del brillo-------------------#
        #pendiente    
        # pendiente queda como calcular la media total
        gamma = 70/meanBrightness
        normal = img.copy()
        normal = exposure.adjust_gamma(normal,gamma)                                            
        #------------------------Normalizacion (y aumento) del brillo-------------------#
        #pendiente    
        #------------------------Segmentacion de las flores-------------------------#
        #OTSU
        imagen=img.copy()
        if(RGB):
            imagen = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(imagen,(5,5),0)
        ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)  
        #Rellenar y separar cada flor
        mascaras= get_one(th3,n_flores)
        
        
        #-------------------------pasar por las x flores que se separaron-----------------------------
        if(RGB):
            archivo = open("Porcetajes_RGB.csv", 'a')
        else:
            archivo = open("Porcetajes_UV.csv", 'a')
        i=0
        for flor in mascaras:
            #---------------------Recortar imagen y aplicar máscara---------------
            imagen_segmentada=recorta_imagen(img, flor)
            #imagen_segmentada = cv2.bitwise_and(img,img, mask=flor)
            string=IDs[i]+","
            i+=1
            if(RGB):
                output = "RGB"+string[:-1]+".png"
            else:
                output = "UV"+string[:-1]+".png"
            #guardar imagen recordad y segmentada
            cv2.imwrite(output, imagen_segmentada)
            #imageio.imsave(output, imagen_segmentada)
            
            #entra imagen enmascarada
            #------------------------Distribucion de colores------------------------------
            #Diferente para uv y RGB
            if RGB:
                porcentajes = Distribucion_Colores(imagen_segmentada) #lista con porcentajes
            else:
                #arreglar treshold según brillo
                 porcentajes = Distribucion_UV(imagen_segmentada)
            #?----------------------Guardar flores cortadas?---------------------------#
            #-----------------------Agregar en tabla o lista---------------------------#        
            
            for color in porcentajes:
                string=string+str(color)+","
            string=string[:-1]
            string=string+"\n"
            archivo.write(string)
        archivo.close()                
print("Ready.")
