import matplotlib.pyplot as plt
import cv2 as cv
import numpy as np

#-----FUNCIONES AUXILIARES-----#

def fill(img):
    #Recibe:
    #   img         Mascara original
    #Retorna:
    #   im_out      Mascara con flores rellenas

    thresh = 50
    im_in = img
    th,im_th = th,im_th = cv.threshold(im_in,200,255,cv.THRESH_BINARY_INV)
    im_floodfill = im_in.copy()
    h, w = im_th.shape[:2]
    mask = np.zeros((h+2,w+2),np.uint8)
    cv.floodFill(im_floodfill,mask,(0,0),255)
    im_floodfill_inv = cv.bitwise_not(im_floodfill)
    im_out = ~(im_th ^ im_floodfill_inv)
    im_out = cv.threshold(im_out, thresh, 255, cv.THRESH_BINARY)[1]
    return im_out

def etiqueta(img_bin):
    #Recibe:
    #   img_bin     Mascara a etiquetar
    #Entrega:
    #   labeled_img Imagen con partes etiquetadas (en escala de grises)
    
    retval,labels,stats,centroids = cv.connectedComponentsWithStats(img_bin)
    #label_hue = np.uint8(179*labels/np.max(labels))
    #blanck_ch = 255*np.ones_like(label_hue)
    #labeled_img = cv.merge([label_hue,blanck_ch,blanck_ch])
    #labeled_img = cv.cvtColor(labeled_img, cv.COLOR_HSV2BGR)
    #labeled_img[label_hue==0]=0
    
    labeled_img = np.uint8(labels)
    
    return labeled_img

def contador(img_label):
    #Recibe:
    #   img_label   Imagen etiquetada en escala de grises
    #Retorna:
    #   elements    Arreglo con valores de etiquetas de los distintos elementos
    
    #labeled_img_2 = cv.cvtColor(img_label, cv.COLOR_BGR2GRAY)
    elements = []
    #flat = labeled_img_2.reshape(-1)
    flat = img_label.reshape(-1)
    
    for i in range(len(flat)):
        pixel_label = flat[i]
        if (pixel_label not in elements):
            elements.append(pixel_label)
    return elements

def one_element(img_label, element):
    # Recibe:
    #    img_label    Una imagen etiquetada en escala de grises
    #    element        El valor de la etiqueta del elemento que se desea aislar 
    # Entrega:
    #    out_img      Una imagen binaria que contiene solamente el elemento deseado
    
    #labeled_img2 = cv.cvtColor(img_label, cv.COLOR_BGR2GRAY)
    #flat = labeled_img2.reshape(-1)
    labeled_img2 = img_label.copy()
    flat = labeled_img2.reshape(-1)
    for i in range(len(flat)):
        pixel_label = flat[i]
        if(pixel_label == element):
            flat[i]=255
        else:
            flat[i]=0
    out_img = flat.reshape(labeled_img2.shape)
    return(out_img)

def position_element(img_bin):
    # Recibe:
        #   img_bin     Imagen binaria, con 1 objeto y fondo negro.
    # Entrega:
        #   position    Valor que indica la posición del elemento del que
        #               se quiere obtener la etiqueta, donde las opciones son:
        #               
        #                    [-1,1]  |  [1,1]
        #                   _________|_________
        #                            |
        #                   [-1,-1]  |  [1,-1]
        #
    # Notas: Esto solo sirve si hay 2 elementos, contando el fondo
    
    retval,labels,stats,centroids = cv.connectedComponentsWithStats(img_bin)
    

    position = [0,0]

    for i in range(len(centroids)-1):
        if (centroids[0,0] < centroids[1,0]): # Si el centro del fondo está a la izquierda del centro de la flor
            position[0] = 1
        else:
            position[0] = -1
        
        if (centroids[0,1] < centroids[1,1]): # Si el centro del fondo está a arriba del centro de la flor
            position[1] = -1
        else:
            position[1] = 1
    
    return position


def ordered_masks(masks,positions):
    # Recibe:
        #   masks       Arreglo de imágenes binarias con 1 flor
        #   positions   Arreglo de posiciones. Cada una indica el cuadrante en 
        #               que se encuentra el elemento de la máscara correspondiente
        #               La forma en que los elementos del arreglo positions
        #               especifican el cuadrante del elemento en la máscara se
        #               muestra a continuación
        #               
        #                    [-1,1]  |  [1,1]
        #                   _________|_________
        #                            |
        #                   [-1,-1]  |  [1,-1]
        #
    # Entrega: 
        #   Ordered     Arreglo de imágenes binarias con 1 flor.
        #               Se encuentran ordenadas de modo que la primera posición
        #               se ubica en el primer cuadrante, la segunda en la segunda, etcétera
        #               Definición de cuadrantes según posición en el arreglo de salida:
        #               
        #                    ordered[1] | ordered[0] 
        #                      _________|_________
        #                               |
        #                    ordered[2] | ordered[3]
        #
    temp = masks.copy()
    n_florws = len(positions)
    cuad = np.zeros(n_florws)
    
    for i in range(n_florws):
        if (positions[i]==[1,1]):
            cuad[i] = 0
        elif (positions[i]==[-1,1]):
            cuad[i] = 1
        elif (positions[i]==[-1,-1]):
            cuad[i] = 2
        elif (positions[i]==[1,-1]):
            cuad[i] = 3
        else:
            cuad[i] = 8 # Error
            
    for i in range(len(masks)):
        temp[i] = masks[int(cuad[i])]
        # Estoy suponiendo que no hay más de una flor en el mismo cuadrante
    Ordered = temp
    return Ordered
        

#-----FUNCION PRINCIPAL-----#

def get_one(mask, n):
    #Recibe:
    #   mask    Mascara de las flores
    #   n       Cantidad de flores en la mascara
    #Retorna:
    #   masks   Arreglo con las mascaras de las flores separadas

    img_fill = fill(mask)
    img_etiquetada = etiqueta(img_fill)
    elementos = contador(img_etiquetada)
    num_elem = len(elementos)
    print("Originalmente hay ", num_elem, " elementos en la imagen")
    eroded = img_etiquetada.copy()
    i=0
    kernel = np.ones((3,3),np.uint8)
    iter = 50
    while(num_elem > n+1):
        i=i+1
        eroded = cv.erode(eroded, kernel, iterations=iter)
        elementos = contador(eroded)
        num_elem = len(elementos)
        print("Tras ", i*iter," erosiones sucesivas, hay ", num_elem, " elementos en la imagen")    
    
    if len(elementos) < n+1:
        eroded2 = img_etiquetada.copy()
        elementos = contador(eroded2)
        num_elem = len(elementos)
        iter = 10
        while(num_elem > n+1):
            i=i+1
            eroded2 = cv.erode(eroded2, kernel, iterations=iter)
            elementos = contador(eroded2)
            num_elem = len(elementos)
            if num_elem <= 10:
                iter = 5
            print("Tras ", i*iter," erosiones sucesivas, hay ", num_elem, " elementos en la imagen")    
        
    
    print("Finalmente hay ", num_elem, " elementos.")
    print(elementos)
    masks = []
    positions = []
    for i in range(num_elem):
        elem = elementos[i]
        if(elem != 0):
            flor = one_element(img_etiquetada,elem)
            flor_pos = position_element(flor)
            masks.append(flor)
            positions.append(flor_pos)
            #plt.imsave('FlorTest_elem'+str(elem)+'.png',flor,cmap='binary_r')
    if len(masks) == 4:
        masks_ord =  ordered_masks(masks,positions)
    else:
        masks_ord = masks.copy()
    return masks_ord
#-----ACA TERMINA LA PARTE PARAMETRIZADA-----#

#CODIGOS DE PRUEBA:
#orig_img = cv.imread('Segmented_Otsu_Gauss_5833.png', cv.IMREAD_GRAYSCALE)
#im_bw = cv.threshold(orig_img, 50, 255, cv.THRESH_BINARY)[1]

#one = get_one(orig_img,4)
