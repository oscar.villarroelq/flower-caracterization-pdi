# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 12:04:52 2023

@author: israt


Segmentación de flores utilizando Otsu's thresholding'
"""


import time
import cv2
import glob
import matplotlib.pyplot as plt


inicio = time.time()
input_path = '\Flores_png'
output_format = '\Flores_png/Segmented_Otsu_'


i = 0
for file in glob.glob(input_path):
    img = cv2.imread(file)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # Otsu's thresholding
    #ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    
    # Otsu's thresholding after Gaussian filtering
    blur = cv2.GaussianBlur(img,(5,5),0)
    ret3,th3 = cv2.threshold(blur,dst,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    
    name = file.split("W_")
    #plt.imsave(output_format+name[1],th2,cmap='binary_r')
    plt.imsave(output_format+'Gauss_'+name[1],th3,cmap='binary_r')
    
    print("Listo con: ",file)
    i = i+1

print("Listoko")
fin = time.time()
print("Tiempo para segmentar ",i, " imágenes: ",int(fin-inicio)," minutos")


