import numpy as np
from skimage.io import imread
import matplotlib.pyplot as plt
from math import sqrt
import pandas as pd

import plotly.express as px
import plotly.graph_objects as go

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

def plot_histogram_rgb(image):
    colors = ("red", "green", "blue")
    channel_ids = (0, 1, 2)
    fig = go.Figure()
    for channel_id, c in zip(channel_ids, colors):
        histogram, bin_edges = np.histogram(image[:, :, channel_id], bins=256, range=(1, 256))
        fig.add_trace(go.Scatter(x=bin_edges[0:-1],  y=histogram, mode='lines',name=c, line_color=c, showlegend=False))
                                 
    fig.update_layout(title="Histograma")
    fig.show()
