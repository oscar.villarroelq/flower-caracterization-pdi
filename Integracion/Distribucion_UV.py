import numpy as np
from skimage.io import imread
import matplotlib.pyplot as plt
import imageio

from math import sqrt
import pandas as pd

def Distribucion_UV(img):
    
    #255>th1>th2>0 
    #th1 y th1 pares
    th1=146
    th2=72

    #valores=[(255-th1)/2 + th1,th1 + (th1-th2)/2,th2/2]
    valores=[255,th1,th2]
    reflexion=["alta","media","baja"]


    clustered_image = np.zeros(img.shape[0]*img.shape[1],dtype=int)
    i=0
    for alto in range(img.shape[0]):
        for largo in range(img.shape[1]):
            if img[alto,largo] != 0:
                if img[alto,largo] >= th1:
                    #clustered_image[i] = (255-th1)/2 + th1
                    clustered_image[i] = 0
                elif img[alto,largo] >= th2:
                    #clustered_image[i]=th1 + (th1-th2)/2
                    clustered_image[i]=1
                else:
                    #clustered_image[i]=th2/2
                    clustered_image[i]=2
            else:
                clustered_image[i]=100
            i+=1

    imagen=img.copy()
    #print(imagen.shape)
    i=0
    for alto in range(imagen.shape[0]):
        for largo in range(imagen.shape[1]):
            #print(img[alto,largo])
            if imagen[alto,largo] != 0:
                imagen[alto,largo]=valores[clustered_image[i]]
                #print(valores[clustered_image[i]])
            i+=1

    contar = [0,0,0,0] #con negro
    for i in clustered_image:
    #print(i)
        if i == 100:
            contar[-1]+=1
        else:
            contar[i]+=1
    #contar

    contar_p=np.float_(contar[:-1])/np.float_(sum(contar[:-1]))*100
    #contar_p

    output = "UV.png"
    imageio.imsave(output, imagen)
    
    return contar_p
#imagen=imread("Flores_png/f2_UV.png")
#imagen = imagen[:,:,0]
#Distribucion_UV(imagen)
