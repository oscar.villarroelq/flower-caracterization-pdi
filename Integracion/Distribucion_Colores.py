import numpy as np
from skimage.io import imread
import matplotlib.pyplot as plt
from math import sqrt
import pandas as pd
import cv2
import os
import rawpy
import imageio
import platform


#inicio = time.time()


clusters_label=["rojo","naranjo","amarillo","verde","celeste","azul","morado","rosado"]
#RGB
#clusters = [[243,22,12],[226, 80, 28],[217, 197, 28],[174, 226, 28],[14, 241, 241],[5, 23, 250],[158, 4, 251],[148, 78, 88]]
#BGR
clusters = [[12,22,243],[28, 80, 226],[28, 197, 217],[28, 226, 174],[241, 241, 14],[250, 23, 5],[251, 4, 158],[88, 78, 148]]

def distancia_euclidiana(A,B):
    return sqrt((A[0]-B[0])**2+(A[1]-B[1])**2+(A[2]-B[2])**2)
    
def distancia_chebyshev(A,B):
    return max(abs(A[0]-B[0]),abs(A[1]-B[1]),abs(A[2]-B[2]))

def distancia_manhatan(A,B):
    return abs(A[0]-B[0])+abs(A[1]-B[1])+abs(A[2]-B[2])

def min_distancia(A):
    distancias = [0,0,0,0,0,0,0,0] #sin negro
    #distancias = [0,0,0,0,0,0,0,0,0] #con negro
    for i in range(len(clusters)):
        distancias[i]=distancia_euclidiana(A,clusters[i])
        #distancias[i]=distancia_chebyshev(A,clusters[i])
        #distancias[i]=distancia_manhatan(A,clusters[i])
    #print(distancias)
    #print(distancias.index(min(distancias)))
    return distancias.index(min(distancias))


def Distribucion_Colores(img,ID):
    img = img[:,:,0:3]
    #designa cada pixel a un clust#dps hay que quitarlo pq se hace afueraer correspondiente
    clustered_image = np.zeros(img.shape[0]*img.shape[1],dtype=int) #indices de a que cluster pertenece cada pixel
    n=0
    for alto in range(img.shape[0]):
        for largo in range(img.shape[1]):
            if img[alto,largo,0] != 0 or img[alto,largo,1] != 0 or  img[alto,largo,2] != 0:
                #calcular a que cluster pertenece
                clustered_image[n]=min_distancia(img[alto,largo,:])
                #print(clustered_image[n],"\n")
            else:
                clustered_image[n]=100
            n+=1
            
    contar = [0,0,0,0,0,0,0,0,0] #sin negro #lista que contiene cuantos pixeles hay de cada cluster
    #contar = [0,0,0,0,0,0,0,0,0,0] #con negro
    for i in clustered_image:
        #print(i)
        if i == 100:
            contar[-1]+=1
        else:
            #suma 1 en posicion correspondiente
            contar[i]+=1
    #print(contar,"\n")

    #proporcion de cada color
    contar_p=np.float_(contar[:-1])/np.float_(sum(contar[:-1]))*100
    #print(contar_p,"\n")

    image=img.copy()
    i=0
    for alto in range(img.shape[0]):
        for largo in range(img.shape[1]):
            #print(img[alto,largo])
            if img[alto,largo,0] != 0 or img[alto,largo,1] != 0 or  img[alto,largo,2] != 0:
                #print(clustered_image[i])
                image[alto,largo]=clusters[clustered_image[i]]
            i+=1

    #image = np.array([image[:,:,2],image[:,:,1],image[:,:,0]])


    _os = platform.system()


    if _os == 'Linux':
        output = "imagenes_cortadas"
    elif _os == 'Darwin':
        output = "imagenes_cortadas"
    elif _os == 'Windows':
        output = "imagenes_cortadas"
            
    output = output+"/RGBQ_"+ID+".png"
    cv2.imwrite(output, image)
    #imageio.imsave(output, image)
    #retornar 
    return contar_p


#imagen = imread('.\imagenes_cortadas/RGB_5.png')
#bin_mask = imread('Mascaras/CRW_5633_mask.png')

#image=imagen.copy()
#image = cv2.bitwise_and(image,image, mask=bin_mask)

#image = image[500:1300,1100:2000]

#porcentajes = Distribucion_Colores(imagen,'1')


