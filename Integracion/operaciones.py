def multiplicativa(imagen,factor):
    i=imagen.copy()
    i*=factor
    i[i>255] = 255
    return i

def sumativa(imagen, factor):
    i=imagen.copy()
    i[:,:,0] = i[:,:,0] + factor
    i[:,:,1] = i[:,:,1] + factor
    i[:,:,2] = i[:,:,2] + factor
    i[i>255] = 255
    return i

