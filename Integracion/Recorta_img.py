# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 20:22:04 2023

@author: israt

Código que recibe una imagen con 1 flor y entrega una imagen más pequeña, en
la que dicha flor se encuentra en el centro, para que no sea necesario iterar
sobre todos los pixeles de la flor original.
"""
import cv2
import numpy as np

from get_one_param import *

def recorta_imagen(orig_image,mask):  # La imagen binaria debe tener solamente 2 objetos
                                      # donde uno de ellos es el fondo y el otro la flor
                                      
    retval,labels,stats,centroids = cv2.connectedComponentsWithStats(mask)
    flower_left,flower_top,flower_width,flower_height,flower_area = stats[1]

    img_2 = np.zeros([flower_height,flower_width,3])

    for h in range(flower_height):
        for w in range(flower_width):
            if(mask[flower_top+h,flower_left+w]==255):
                #img_2[h,w]=orig_image[flower_top+h,flower_left+w]
                img_2[h,w]=[orig_image[flower_top+h,flower_left+w,2],orig_image[flower_top+h,flower_left+w,1],orig_image[flower_top+h,flower_left+w,0]]
            else:
                img_2[h,w]=[0,0,0]
    #img_2 = cv2.cvtColor(img_2, cv2.COLOR_BGR2RGB)
    return img_2

"""
#img = imread('.\Flores_png/CRW_5633_brillo.png')
dir = 'D:\PDI_DATASET\etiquetadas/' #carpeta donde se encuentran las imágenes a precesar
fotos = os.listdir(dir)
foto = 'RGB_5_6_7_8.CRW'
#aux = foto.split(".") #aux contiene nombre del archivo y extencion [<nombre>,CRW]
if foto.endswith(".CRW"):
    #foto a leer
    foto_dir = dir+foto
    print(foto_dir)
    with rawpy.imread(foto_dir) as raw:
       rgb = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
        


#mask =  cv2.imread('\Mascaras/CRW_5633_mask.png', cv2.IMREAD_GRAYSCALE)
img = rgb[:,:,0:3]
imagen=img.copy()
imagen = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(imagen,(5,5),0)
ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)  
#Rellenar y separar cada flor
mascaras = get_one(th3,4)

una_flor = recorta_imagen(img, mascaras[1])
        
cv2.imwrite('Solo_la_flor_cv2.jpg', una_flor)
"""
