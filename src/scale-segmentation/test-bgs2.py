import cv2
import numpy as np
import rawpy
from matplotlib import pyplot as plt

#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
bg_subtractor = cv2.bgsegm.createBackgroundSubtractorCNT()

img_in = "dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
with rawpy.imread(img_in) as raw:
	reference_image = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
reference_image = cv2.cvtColor(reference_image, cv2.COLOR_BGR2RGB)
reference_gray = cv2.cvtColor(reference_image, cv2.COLOR_BGR2GRAY)
fgmask = bg_subtractor.apply(reference_gray)
#fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)


# Lista de nombres de archivos de las otras imágenes
image_dir = "dataset/Individuo77-199-1-40/Visible/"
image_files = ['CRW_5821.CRW', 'CRW_5822.CRW', 'CRW_5823.CRW']  # Agrega tus nombres de archivos

# Procesar cada imagen
for image_file in image_files:
    # Cargar la imagen actual
    image_file = image_dir+image_file
    print(image_file)
    with rawpy.imread(image_file) as raw:
        current_image = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
    current_image = cv2.cvtColor(current_image, cv2.COLOR_BGR2RGB)
    current_gray = cv2.cvtColor(current_image, cv2.COLOR_BGR2GRAY)
    fgmask = bg_subtractor.apply(current_gray)
    #fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    

    # Crear una máscara negra del mismo tamaño que la imagen actual
    mask = np.zeros_like(fgmask)

    # Dibujar los contornos encontrados en la máscara
    #cv2.drawContours(mask, contours, -1, (255), thickness=cv2.FILLED)

    # Eliminar el fondo de la imagen actual usando la máscara
    result = cv2.bitwise_and(current_image, current_image, mask=fgmask)

    # Mostrar las imágenes
    plt.subplot(131), plt.imshow(cv2.cvtColor(current_image, cv2.COLOR_BGR2RGB)), plt.title('Original')
    #plt.subplot(132), plt.imshow(thresholded, cmap='gray'), plt.title('Thresholded Difference')
    plt.subplot(132), plt.imshow(fgmask, cmap='gray'), plt.title('Thresholded Difference')
    plt.subplot(133), plt.imshow(cv2.cvtColor(result, cv2.COLOR_BGR2RGB)), plt.title('Result')
    plt.show()