import cv2
import rawpy
import numpy as np
import matplotlib.pyplot as plt

#img_in = "../dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
img_in = "dataset/Individuo77-199-1-40/Visible/CRW_6080.CRW"
with rawpy.imread(img_in) as raw:
	img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5,5), 0)
adjusted = cv2.convertScaleAbs(blur, 3, 10)
thresh = cv2.threshold(adjusted, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
thresh = 255 - thresh

kernel = np.ones((800,70), np.uint8)
morph = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

# test contornos
#contours, hierarchy = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#contours = contours[0] if len(contours) == 2 else contours[1]

# guarda resultados
masked = gray.copy()
morph = cv2.bitwise_not(morph)
masked = cv2.bitwise_and(masked, masked, mask = morph)
#cv2.putText(maskTEST, 'Mask', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 0), 2)
print("masked shape ", masked.shape)
nonZero = masked[np.where(masked != 0)]
#nonZeroMask = np.all(masked !=0 , axis=-1)
#nonZero = masked[nonZeroMask]

print("nonzero shape ",nonZero.shape)
meanBrightness = np.mean(nonZero)

normalized = img.copy()
normal = cv2.normalize(normalized, None, alpha=100/meanBrightness, beta=0, norm_type = cv2.NORM_INF)
print(normal.shape)
print(meanBrightness)
# muestra resultados

img = cv2.resize(img, (1600,900), interpolation=cv2.INTER_AREA)
thresh = cv2.resize(thresh, (1600,900), interpolation=cv2.INTER_AREA)
morph = cv2.resize(morph, (1600,900), interpolation=cv2.INTER_AREA)
#result = cv2.resize(result, (1600,900), interpolation=cv2.INTER_AREA)
masked = cv2.resize(masked, (1600,900), interpolation=cv2.INTER_AREA)
normal = cv2.resize(normal, (1600,900), interpolation=cv2.INTER_AREA)
#nonZero = cv2.resize(nonZero, (500,900), interpolation=cv2.INTER_AREA)

cv2.imshow("img", img)
#cv2.imshow("thresh", thresh)
#cv2.imshow("morph", morph)
#cv2.imshow("result", result)
cv2.imshow("masked",masked)
#cv2.imshow("normal",normal)
#cv2.imshow("nonZero",nonZero)

cv2.waitKey(0)
cv2.destroyAllWindows()

"""
# Visualizar la imagen original
plt.subplot(1, 2, 1)
plt.title('Imagen Original')
plt.imshow(masked)

# Visualizar solo los píxeles no cero
plt.subplot(1, 2, 2)
plt.title('Píxeles No Cero')
plt.imshow(nonZero)

plt.show()
"""