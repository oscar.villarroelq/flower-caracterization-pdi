import cv2
import rawpy
import numpy as np
from matplotlib import pyplot as plt

img_in = "dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
with rawpy.imread(img_in) as raw:
	image = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
#image = cv2.imread('imagen.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

bg_subtractor = cv2.bgsegm.createBackgroundSubtractorCNT()
foreground_mask = bg_subtractor.apply(gray)

result = cv2.bitwise_and(image, image, mask=foreground_mask)

# Mostrar las imágenes
plt.subplot(131), plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB)), plt.title('Original')
plt.subplot(132), plt.imshow(foreground_mask, cmap='gray'), plt.title('Foreground Mask')
plt.subplot(133), plt.imshow(cv2.cvtColor(result, cv2.COLOR_BGR2RGB)), plt.title('Result')
plt.show()