import cv2
import rawpy
import numpy as np
import random as rng

img_in = "../dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
with rawpy.imread(img_in) as raw:
	img = raw.postprocess()

img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#img_thresh = cv2.threshold(img_gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#img_gray = cv2.GaussianBlur(img_gray, (5,5), 0)
img_gray = cv2.bilateralFilter(img_gray,9,75,75)
img_canny = cv2.Canny(img_gray, 150, 200)
temp = cv2.resize(img_canny, (1600,900), interpolation=cv2.INTER_AREA)
cv2.imshow('temp', temp)

contours, hierarchy = cv2.findContours(img_canny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

hull_list = []
for i in range(len(contours)):
	hull = cv2.convexHull(contours[i])
	hull_list.append(hull)

 # Draw contours + hull results
drawing = np.zeros((img_canny.shape[0], img_canny.shape[1], 3), dtype=np.uint8)
for i in range(len(contours)):
	color = (rng.randint(0,256), rng.randint(0,256), rng.randint(0,256))
	cv2.drawContours(drawing, contours, i, color)
	cv2.drawContours(drawing, hull_list, i, color) 
# Show in a window
drawing = cv2.resize(drawing, (1600,900), interpolation=cv2.INTER_AREA)
cv2.imshow('Contours', drawing)

cv2.waitKey(0)
cv2.destroyAllWindows
