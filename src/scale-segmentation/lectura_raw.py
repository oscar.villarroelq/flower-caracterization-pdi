import os
import rawpy
import imageio

dir = 'D:\PDI_DATASET/'
fotos = os.listdir(dir)
print(fotos)
for img in fotos:
    print(img)
    aux = img.split(".")
    if img.endswith(".CRW"):
        #foto a leer
        foto_dir = dir+img
        print(foto_dir)
        temperature = 5500  # Adjust to your preference
        tint = 0  # Adjust to your preference
        brightness = 0.5  # Adjust to your preference
        contrast = 1.2  # Adjust to your preference
        saturation = 1  # Adjust to your preference
        sharpness = 0.5  # Adjust to your preference
        with rawpy.imread(foto_dir) as raw:
            #rgb = raw.postprocess(no_auto_bright=True, output_bps=16)
                        rgb = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
        output = aux[0]+".png"
        imageio.imsave(output, rgb)
    print(output + " ready")
print("Ready.")

