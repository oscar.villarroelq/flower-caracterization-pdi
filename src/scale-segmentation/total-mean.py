import os
import rawpy
import imageio
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
from skimage import io, color, exposure

start = time.time()
dir = 'dataset/Individuo77-199-1-40/Visible/'
fotos = os.listdir(dir)
#print(fotos)
recgn = []
meanRecgn = []
kernel = np.ones((700,70), np.uint8)

for img_ in fotos:
	#print(img)
    if img_.endswith(".CRW"):
        foto_dir = dir+img_
        with rawpy.imread(foto_dir) as raw:
            img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5,5), 0)
        adjusted = cv2.convertScaleAbs(blur, 3,10)
        thresh = cv2.threshold(adjusted, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
        thresh = 255 - thresh
        morph = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        contours, hierarchy = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        masked = gray.copy()
        morph = cv2.bitwise_not(morph)
        masked = cv2.bitwise_and(masked, masked, mask = morph)
        nonZero = masked[masked != 0]
        meanBrightness = np.mean(nonZero)
        meanRecgn.append(meanBrightness)
        #print(meanBrightness)

totalMeanBright = np.average(meanRecgn) 
totalVarBright = np.var(meanRecgn)
"""
for img_ in fotos:
    if img_.endswith(".CRW"):
        foto_dir = dir+img_
        with rawpy.imread(foto_dir) as raw:
            img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
        imagen_gris = color.rgb2gray(img)
        gamma = totalMeanBright/np.mean(imagen_gris)
        img_normalizada = exposure.adjust_gamma(img,gamma)
"""
    
print("media= ",totalMeanBright, "var = ", totalVarBright)
np.savetxt("bright-test.csv", meanRecgn, delimiter=",")
print("El programa tarda ", time.time()-start)