import cv2
import rawpy
import numpy as np

img_in = "../dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
with rawpy.imread(img_in) as raw:
	img = raw.postprocess()
# convert to grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# threshold
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]

# invert
thresh = 255 - thresh

# apply horizontal morphology close
kernel = np.ones((400,70), np.uint8)
morph = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

# get external contours
contours, hierarchy = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#contours = contours[0] if len(contours) == 2 else contours[1]

print(len(contours),len(hierarchy))
# draw contours
result = img.copy()
temp = []
for cntr in contours:
	temp.append(cntr)
	approx = cv2.approxPolyDP(cntr, 0.01*cv2.arcLength(cntr, True), True)
	print("approx: ",len(approx))
	print("pos: ", cntr[0][0])
	if(len(approx) == 4):
		# get bounding boxes
		x1,y1 = cntr[0][0]
		pad = 10
		x,y,w,h = cv2.boundingRect(cntr)
		print(x,y,w,h)
		cv2.rectangle(result, (x-pad, y-pad), (x+w+pad, y+h+pad), (0, 0, 255), 4)

# save result
#cv2.imwrite("john_bbox.png",result)

maskTEST = img.copy()
morph = cv2.bitwise_not(morph)
maskTEST = cv2.bitwise_and(maskTEST, maskTEST, mask = morph)
cv2.putText(maskTEST, 'Mask', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 0), 2)


"""
for largo in range(morph.shape[0]):
	for ancho in range(morph.shape[1]):
		#print(sum(bin_mask[largo,ancho]))
		if morph[largo,ancho] != 0:
			#print("a")
			#print(bin_mask[largo,ancho])
			maskTEST[largo,ancho,0]=0
			maskTEST[largo,ancho,1]=0
			maskTEST[largo,ancho,2]=0
"""

# display result
thresh = cv2.resize(thresh, (1600,900), interpolation=cv2.INTER_AREA)
morph = cv2.resize(morph, (1600,900), interpolation=cv2.INTER_AREA)
result = cv2.resize(result, (1600,900), interpolation=cv2.INTER_AREA)
maskTEST = cv2.resize(maskTEST, (1600,900), interpolation=cv2.INTER_AREA)

#cv2.imshow("thresh", thresh)
#cv2.imshow("morph", morph)
#cv2.imshow("result", result)
#cv2.imshow("masked",maskTEST)

cv2.waitKey(0)
cv2.destroyAllWindows()
