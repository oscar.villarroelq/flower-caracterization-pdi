import rawpy
import imageio
import cv2
img_in_uv = "CRW_5917-uv.CRW"
img_in_vs = "CRW_5820-vs.CRW"
with rawpy.imread(img_in_vs) as raw:
	try:
		thumb = raw.extract_thumb()
		#print(thumb)
		#cv2.imshow(thumb)
		#cv2.waitKey(0)
	except rawpy.LibRawNoThumbnailError:
    		print('no thumbnail found')
	except rawpy.LibRawUnsupportedThumbnailError:
    		print('unsupported thumbnail')

	else:
		if thumb.format == rawpy.ThumbFormat.JPEG:
			with open('thumb.jpg', 'wb') as img:
				img.write(thumb.data)
				cv2.imshow('img', img)
				cv2.waitKey(0)
		elif thumb.format == rawpy.ThumbFormat.BITMAP:
			img = cv2.imread('thumb.tiff')
			cv2.imshow(img)
			cv2.waitKey(0)


#gris = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
"""
img_uv = rawpy.imread(img_in_uv)
img_vs = rawpy.imread(img_in_vs)

print(img_uv.color_desc)
print(img_vs.color_desc)

img_raw_uv = img_uv.raw_colors
print("type: ",img_uv.raw_type)
#print("color matrix", image.color_matrix)
#cv2.imshow("aaa",image.color_matrix)
cv2.imshow('imagen',img_raw_uv)
cv2.waitKey(0)
"""
#output_image_path = "test2.png"
#imageio.imsave(output_image_path, rgb)
cv2.destroyAllWindows()
