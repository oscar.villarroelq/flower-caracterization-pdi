import os
import rawpy
import imageio
import cv2
import numpy as np
import time

start = time.time()
dir = '../dataset/Individuo77-199-1-40/Visible/'
fotos = os.listdir(dir)
#print(fotos)
recgn = []
for img_ in fotos:
	#print(img)
	if img_.endswith(".CRW"):
		foto_dir = dir+img_
		with rawpy.imread(foto_dir) as raw:
			img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
		img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		img_blur = cv2.GaussianBlur(img_gray, (3,3), 0)
		ret,thresh = cv2.threshold(img_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
		contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		for cnt in contours:
			approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt, True), True)
			sides = len(approx)
			if sides == 4:
				x, y, w, h = cv2.boundingRect(cnt)
				if h > 400 | w > 400:
					recgn.append(img_)

print("El programa tarda ", time.time()-start)
print("En total se procesaron ",len(fotos)," imagenes." )
print("Hay ", len(fotos)-len(recgn), " donde no se reconoce")
not_recg = []
for img in fotos:
	if img not in recgn:
		not_recg.append(img)
print("Fotos sin regla reconocida: ", not_recg)
