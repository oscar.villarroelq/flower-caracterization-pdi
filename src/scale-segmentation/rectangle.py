import cv2
import rawpy
import imageio
import numpy as np

#img_in = "CRW_5822-vs.CRW"
#img_in = "../dataset/Individuo77-199-1-40/Visible/CRW_5820.CRW"
#img_in = "../dataset/Individuo1-76/CRW_5624.CRW"
img_in = "CRW_5820.CRW"
with rawpy.imread(img_in) as raw:
	"""
	thumb = raw.extract_thumb()
	with open('thumb.jpg', 'wb') as file:
		file.write(thumb.data)
	img = cv2.imread('thumb.jpg')
	"""
	#img = raw.raw_image_visible.copy()
	#print(img.shape)
	img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
	#print(raw.shapes)
img_init = img
temp_init = cv2.resize(img_init, (1600,900), interpolation=cv2.INTER_AREA)
cv2.imshow("Original",temp_init)
""" sube el brillo para imagenes uv, pero no reconoce muy bien y se demora siglos
image = img
new_image = np.zeros(image.shape, image.dtype)
for y in range(image.shape[0]):
	for x in range(image.shape[1]):
		for c in range(image.shape[2]):
			new_image[y,x,c] = np.clip(2*image[y,x,c] + 100, 0, 255)
img = new_image
"""
# foto gris
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
temp1 = cv2.resize(img_gray, (1600,900), interpolation=cv2.INTER_AREA)
cv2.imshow("Gris",temp1)
cv2.waitKey(0)
# blurring
#img_blur = cv2.medianBlur(img_gray, 5)
img_blur = cv2.GaussianBlur(img_gray, (13,13), 0)
temp2 = cv2.resize(img_blur, (1600,900), interpolation=cv2.INTER_AREA)
#cv2.imshow("Ruido",temp2)
#cv2.waitKey(0)
# threshold
#img_canny = cv2.Canny(img_blur, 10, 50)
#img_blur = img_canny

#ret,thresh = cv2.threshold(img_blur,127,255,cv2.THRESH_BINARY)
#thresh = cv2.adaptiveThreshold(img_blur,127,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
#thresh = cv2.adaptiveThreshold(img_blur,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
ret,thresh = cv2.threshold(img_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

temp = cv2.resize(thresh, (1600,900), interpolation=cv2.INTER_AREA)
#cv2.imshow("Threshold",temp)
#cv2.waitKey(0)

#print(thresh.shape)

# reconocimiento de contornos
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
#contours, hierarchy = cv2.findContours(thresh, 1, 2)
sqrs = []
rcts = []


for cnt in contours:
	x1,y1 = cnt[0][0]
	approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt, True), True)
	sides = len(approx)
	if (sides == 4) | (sides == 5):
		x, y, w, h = cv2.boundingRect(cnt)
		if (w > 200) | (h > 200): # cuadrados o rectangulos son de tamaño considerable
			ratio = float(w)/h
			if ratio >= 0.9 and ratio <= 1.1:
				# guarda mask cuadrados
				sqrs.append(cnt)
				# agrega contorno a la imagen original
				img = cv2.drawContours(img, [cnt], 0, (0,255,255), 3)
				cv2.putText(img, 'Cuadrado', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 0), 2)
			else:
				rcts.append(cnt) # guarda mask rectangulos
				img = cv2.drawContours(img, [cnt], 0, (0,255,0), 3)
				#img = cv2.drawContours(img, [cnt], 0, (0,255,0), cv2.FILLED)
				#cv2.putText(img, 'Rectangle', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
				cv2.putText(img, 'Escala Grises', (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)


## intento de separar la mascara

masked = np.zeros(img_init.shape)
temp = cv2.drawContours(masked,[rcts[0]],0,(0,255,0), cv2.FILLED)
temp_gray = cv2.cvtColor(np.uint8(temp), cv2.COLOR_BGR2GRAY)
aux1,aux2 = rcts[0][0][0]
cv2.putText(temp_gray, 'Mascara', (aux1,aux2), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
cv2.imshow("test1",cv2.resize(temp_gray, (1600,900), interpolation=cv2.INTER_AREA ))
th3 = cv2.adaptiveThreshold(temp_gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
cv2.putText(th3, 'Mascara', (aux1,aux2), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
cv2.imshow("test2",cv2.resize(th3, (1600,900), interpolation=cv2.INTER_AREA ))
cv2.waitKey(0)


img = cv2.resize(img, (1600,900), interpolation=cv2.INTER_AREA)
cv2.imwrite("rectangle-detection-findContours.png",img)
cv2.imshow("image",img)
cv2.waitKey(0)
cv2.destroyAllWindows()
