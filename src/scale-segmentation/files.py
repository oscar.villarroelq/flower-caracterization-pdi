import os
import rawpy
import imageio

dir = '/home/oscarillo/Documents/PDI/dev/dataset/Individuo77-199-1-40/'
fotos = os.listdir(dir)
print(fotos)
for img in fotos:
	print(img)
	aux = img.split(".")
	if img.endswith(".CRW"):
		#foto a leer
		foto_dir = dir+img
		with rawpy.imread(foto_dir) as raw:
			rgb = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True)
		output = aux[0]+".png"
		imageio.imsave(output, rgb)
	print(output + " ready")
print("Ready.")
