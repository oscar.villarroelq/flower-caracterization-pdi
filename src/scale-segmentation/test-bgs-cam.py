import numpy as np
import cv2 as cv
import time
#cap = cv.VideoCapture('vtest.avi')
#_ = cv.VideoCapture("/dev/video0")
cap = cv.VideoCapture("/dev/video0")
kernel = cv.getStructuringElement(cv.MORPH_RECT,(3,3))
fgbg = cv.bgsegm.createBackgroundSubtractorGMG()
while(1):
    ret, frame = cap.read()
    print(frame.shape)
    frame = cv.cvtColor(frame, cv.COLOR_RGB2GRAY)
    blur = cv.GaussianBlur(frame, (5,5), 0)
    thresh = cv.threshold(blur, 0, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)[1]
    #fgmask = fgbg.apply(frame)
    frame = cv.morphologyEx(thresh, cv.MORPH_GRADIENT, kernel)
    #temp = 255-fgmask
    #cv.imshow('frame',cv.bitwise_and(frame,frame,mask=fgmask))
    cv.imshow('aaa', frame)
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
cap.release()
cv.destroyAllWindows()