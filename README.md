# Caracterización de flores

Desarrollo de aplicación para caracterización de flores

## Como funciona
<img src="docs/diagram1.jpeg" title="" alt="diagram1.jpeg" width="635">

## Requisitos del sistema
- Python
- Jupyter Notebook

## Requisitos del dataset
- Imagenes en formato .CRW
- Las imagenes deben tener un fondo monocromático considerablemente distinto a las flores
- Debe haber una escala de grises para ajustar brillo
- El nombre de la foto debe contener los ID de las flores que se encuentran en dicho archivo, en un orden específico mostrado en la figura siguiente
- El nombre además debe decir que corresponde a una imagen tipo RGB

<img src="docs/ejemplo_orden.png" title="" alt="ejemplo_orden.png" width="635">


## Como se usa

- Descargar o clonar proyecto.
- Instalar bibliotecas requeridas. 
- Abrir el archivo Integracion >> Caracterisation-app.ipynb
- Dentro de la función "Integracion" (3ra celda de código) configurar la dirección la carpeta que contiene las imagenes a procesar.

## Salidas del sistema
- Esta aplicación crea distintos archivos, como dos .csv para guardar los resultados de la caracterización e imagenes con las flores para revisar los resultados en la visualización de los datos. 

## Consideraciones adicionales
- Para mostrar los resultados se utiliza la funcion interact, la cual a veces tiene problemas para mostrar los gráficos.

## Procesamiento digital de imagenes - ELO328

## Integrantes
- Joaquín Dubo
- Felipe Mejias
- David Tapia
- Natalia Vega
- Oscar Villarroel
